
var arrayPizzas = new Array
var linhasTabela


var btnGravar = document.querySelector("#btnGravar")

btnGravar.addEventListener("click", function (event) {

    let formulario = document.querySelector("#idFormulario")

     if (formulario.nmNomeComercial.value == 0) {
        alert("Favor inserir nome da pizza")
    }if (formulario.nmTamanho.value == 0) {
        alert("Favor inserir tamanho da pizza")
    }if (formulario.nmPreco.value == 0) {
        alert("Favor inserir preço da pizza")
    } else{
        let pizza = obterDadosPizza(formulario)
        let precoEmcm = calcularPrecoEmCm(pizza)
        let cadastrarArray = gravarNoArray(pizza, precoEmcm)
    
        linhasTabela = montarTr(arrayPizzas)
    
        document.getElementById("idFormulario").reset()
    }
})


btnGerarRelatorio.addEventListener("click",function(event) {
    
    if (arrayPizzas.length == 0 || arrayPizzas == 0) {
        alert("Nenhuma pizza cadastrada")
    }else{
        
        let montaTabela = document.getElementById("idTbody")

        for (let index = 0; index < linhasTabela.length; index++) {
            montaTabela.appendChild(linhasTabela[index])
        }       
        let desabilitaBtn = document.getElementById("btnGerarRela")
        desabilitaBtn.setAttribute("class", "btn btn-secondary disabled")
    }
})


function obterDadosPizza(formulario) {
    let dadosPizza = {
        nome: formulario.nmNomeComercial.value,
        tamanho: parseFloat(formulario.nmTamanho.value),
        preco: formulario.nmPreco.value,
    }
    return dadosPizza
}

function montarTr(arrayPizzas) {

    let arrayTableTr = new Array
    let tableTr
    for (let index = 0; index < arrayPizzas.length; index++) {
        tableTr = document.createElement("tr")
        tableTr.appendChild(montarTd(arrayPizzas[index].nome))
        tableTr.appendChild(montarTd(arrayPizzas[index].tamanho + " cm "))
        tableTr.appendChild(montarTd("R$ " + arrayPizzas[index].preco))
        tableTr.appendChild(montarTd("R$ " + arrayPizzas[index].precoEmcm.toFixed(2)))
        if (index == 0) {
            tableTr.appendChild(montarTd(arrayPizzas[index].custoBen = "Melhor custo Benefício"))
        } if (index != 0) {
            tableTr.appendChild(montarTd(
                arrayPizzas[index].custoBen = (
                    (arrayPizzas[index].precoEmcm  - arrayPizzas[index - 1].precoEmcm)
                     /  arrayPizzas[index - 1].precoEmcm * 100 ).toFixed(2) + " %") )
        }
        arrayTableTr.push(tableTr)
    }
    return arrayTableTr
}

function montarTd(pizza) {
    let tableTD = document.createElement("td")
    tableTD.textContent = pizza
    return tableTD
}

function gravarNoArray(pizza, precoEmcm) {

    let pizzas = {
        nome: pizza.nome,
        tamanho: pizza.tamanho,
        preco: pizza.preco,
        precoEmcm: precoEmcm,
        custoBen: ""
    }

    let filtraNome = arrayPizzas.filter(arrayPizzas => arrayPizzas.nome == pizza.nome)

    if (filtraNome.length == 0) {
        arrayPizzas.push(pizzas)

        arrayPizzas.sort(function (a, b) {
        return a.precoEmcm - b.precoEmcm
    })
    } else{
        alert("Pizza já cadastrada")
    }
}

function calcularPrecoEmCm(dadosPizza) {

    let pi = 3.14
    let raio = (dadosPizza.tamanho / 2)
    let area = pi * (raio * raio)
    let precoCm = dadosPizza.preco / area
 
    return parseFloat(precoCm.toFixed(6))
}


function removerCollapse(){

    let div = document.getElementById("collapseExample")
    if(div.className == "collapse"){
        div.classList.remove("collapse")
    }else{
        div.classList.add("collapse")
    }
}

